from django.db import models


class Account(models.Model):
    name = models.CharField(
        max_length=255,
        verbose_name='Account Name'
    )

class Campaign(models.Model):
    name = models.CharField(
        max_length=255,
        verbose_name='Ad Group Name'
    )

    account = models.ForeignKey(
        Account,
        related_name='campaigns',
        verbose_name='Ad Group Account',
        on_delete=models.DO_NOTHING,
    )

class AdGroup(models.Model):
    name = models.CharField(
        max_length=255,
        verbose_name='Ad Group Name'
    )

    campaign = models.ForeignKey(
        Campaign,
        related_name='adgroups',
        on_delete=models.DO_NOTHING,
    )


class Keyword(models.Model):

    keyword = models.CharField(
        max_length=255,
        verbose_name='Keyword'
    )


class Impressions(models.Model):

    ad_group = models.ForeignKey(
       AdGroup,
       related_name='adgroup_impressions',
       verbose_name='adgroup',
       on_delete=models.DO_NOTHING,
    )

    keyword = models.ForeignKey(
       Keyword,
       related_name='impressions',
       verbose_name='keyword',
       on_delete=models.DO_NOTHING,
    )

    date = models.DateField(
        verbose_name='date',
    )

    clicks = models.IntegerField(
        verbose_name='Clicks'
    )

    impressions = models.IntegerField(
        verbose_name='Impressions'
    )

    prediction = models.BooleanField(default=False)




class Weather(models.Model):
    date = models.DateField(
        verbose_name='date',
    )
    temperature = models.FloatField(
        verbose_name='temperature',
    )
    uv_index = models.IntegerField(
        verbose_name='uv index',
    )
    humidity = models.FloatField(
        verbose_name='humidity'
    )
