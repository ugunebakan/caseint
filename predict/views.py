from django.utils import timezone
from rest_framework import viewsets
from predict.models import Impressions, Account
from predict.serializers import AccountSerializer
from predict.filters import AccountFilter
from django_filters.rest_framework import DjangoFilterBackend
from django.db.models.query import Prefetch


class AccountViewSet(viewsets.ModelViewSet):

    queryset = Account.objects.all()

    serializer_class = AccountSerializer
    filter_class = AccountFilter
    filter_backends = (DjangoFilterBackend,)

    def get_queryset(self):
        if not any(x in ['date', 'account']
                   for x in self.request.query_params.keys()):
            queryset = Account.objects.prefetch_related(
                'campaigns', 'campaigns__adgroups',
                Prefetch(
                    'campaigns__adgroups__adgroup_impressions',
                    queryset=Impressions.objects.filter(
                        date__gte=timezone.now())),
                'campaigns__adgroups__adgroup_impressions__keyword',)
        else:
            queryset = self.queryset

        return queryset
