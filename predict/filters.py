import django_filters
from django_filters.rest_framework import FilterSet
from predict.models import Account, Impressions
from django.db.models.query import Prefetch


class AccountFilter(FilterSet):
    date = django_filters.DateTimeFilter(
        field_name='date', method='filter_method')
    account = django_filters.NumberFilter(
        field_name='account', method='filter_method')

    def filter_method(self, queryset, name, value):
        if name == 'date':
            queryset = queryset.prefetch_related(
                'campaigns',
                'campaigns__adgroups',
                Prefetch(
                    'campaigns__adgroups__adgroup_impressions',
                    queryset=Impressions.objects.filter(date=value)
                ),
                'campaigns__adgroups__adgroup_impressions__keyword',
            ).distinct()
        elif name == 'account':
            queryset = queryset.prefetch_related(
                'campaigns',
                'campaigns__adgroups',
                'campaigns__adgroups__adgroup_impressions',
                'campaigns__adgroups__adgroup_impressions__keyword',
            ).filter(id=value)

        return queryset


    class Meta:
        model = Account
        fields = [
            'date',
            'account',
        ]
