from rest_framework import routers
from predict.views import AccountViewSet


router = routers.SimpleRouter()
router.register(r'', AccountViewSet, base_name='impressions')

urlpatterns = router.urls
