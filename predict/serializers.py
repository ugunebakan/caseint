from rest_framework import serializers
from predict.models import (
    Impressions,
    Account,
    Campaign,
    AdGroup,
    Keyword,
)


class ImpressionSerializer(serializers.ModelSerializer):
    keyword = serializers.ReadOnlyField(source='keyword.keyword')

    class Meta:
        model = Impressions
        fields = '__all__'


class AdGroupSerializer(serializers.ModelSerializer):
    keywords = ImpressionSerializer(many=True, source='adgroup_impressions')

    class Meta:
        model = AdGroup
        fields = '__all__'


class CampainSerializer(serializers.ModelSerializer):
    adgroups = AdGroupSerializer(many=True)

    class Meta:
        model = Campaign
        fields = '__all__'


class AccountSerializer(serializers.ModelSerializer):
    campaigns = CampainSerializer(many=True)

    class Meta:
        model = Account
        fields = '__all__'
