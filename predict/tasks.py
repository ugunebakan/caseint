from datetime import timedelta, date
import numpy as np
from sklearn.ensemble import RandomForestRegressor
from sklearn.multioutput import MultiOutputRegressor
from celery import shared_task
from predict.helpers import forecast_weather
from predict.models import (
    Account,
    Weather,
    Impressions,
)
import logging
logger = logging.getLogger(__name__)


def _daterange(start_date):
    for n in range(7):
        yield start_date + timedelta(n)


def _get_weather_info(date):
    weather_values = Weather.objects.filter(date=date).values_list(
        'temperature',
        'humidity',
    )
    return [list(value) for value in weather_values][0]


def _tupple_to_list(_tupple):
    _list = list(_tupple)
    _date = _list[-1]

    weather_values = _get_weather_info(_date)
    return _list[:-1] + weather_values


@shared_task
def update_predictions():
    logger.info('Update Predictions started')
    for account in Account.objects.all():
        logger.debug('account {0}'.format(account))
        data = account.campaigns.filter(
            adgroups__adgroup_impressions__prediction=False
        ).values_list(
            'id',  # campaign id
            'adgroups__id',  # adgroup id
            'adgroups__adgroup_impressions__keyword',  # keyword id
            'adgroups__adgroup_impressions__clicks',  # keyword clicks
            'adgroups__adgroup_impressions__impressions',  # keyword impressio
            'adgroups__adgroup_impressions__date'  # date
        )

        values_set = np.array([_tupple_to_list(sing) for sing in data])

        output_set = values_set[:, [3, 4]]  # clicks and impressions as outputs
        # campaign_id, adgroup_id etc.
        feature_set = values_set[:, [0, 1, 2, 5, 6]]

        regr_multirf = MultiOutputRegressor(RandomForestRegressor(
            n_estimators=100,
            max_depth=30,
            random_state=0,
        ))

        regr_multirf.fit(feature_set, output_set)
        logger.debug('model created')
        for single_date in _daterange(date.today()):
            # remove previous predictions since weather conditions might
            # change
            ad_group_ids = list(set(account.campaigns.all().values_list('adgroups__id', flat=True)))
            Impressions.objects.filter(date=single_date, prediction=True, ad_group__id__in=ad_group_ids).delete()
            logger.debug('prediction for {0}'.format(single_date))
            prediction_set = feature_set
            forecast = forecast_weather(single_date)
            values = [
                forecast.get('currently').get('temperature'),
                forecast.get('currently').get('humidity')]
            prediction_set[:, 3] = values[0]
            prediction_set[:, 4] = values[1]

            prediction_results = regr_multirf.predict(
                prediction_set).round().astype(int)

            prediction = np.concatenate(
                (prediction_set, prediction_results),
                axis=1).astype(int)
            top_list = [
                Impressions(
                    ad_group_id=row[1],
                    keyword_id=row[2],
                    date=single_date, clicks=row[-2],
                    impressions=row[-1],
                    prediction=True) for row in prediction]

            Impressions.objects.bulk_create(top_list)
