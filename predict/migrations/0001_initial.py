# Generated by Django 2.1.5 on 2019-01-29 20:31

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Account',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='Account Name')),
            ],
        ),
        migrations.CreateModel(
            name='AdGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='Ad Group Name')),
            ],
        ),
        migrations.CreateModel(
            name='Campaign',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='Ad Group Name')),
                ('account', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='campaigns', to='predict.Account', verbose_name='Ad Group Account')),
            ],
        ),
        migrations.CreateModel(
            name='Impressions',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(verbose_name='date')),
                ('clicks', models.IntegerField(verbose_name='Clicks')),
                ('impressions', models.IntegerField(verbose_name='Impressions')),
                ('prediction', models.BooleanField(default=False)),
                ('ad_group', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='adgroup_impressions', to='predict.AdGroup', verbose_name='adgroup')),
            ],
        ),
        migrations.CreateModel(
            name='Keyword',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('keyword', models.CharField(max_length=255, verbose_name='Keyword')),
            ],
        ),
        migrations.CreateModel(
            name='Weather',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(verbose_name='date')),
                ('temperature', models.FloatField(verbose_name='temperature')),
                ('uv_index', models.IntegerField(verbose_name='uv index')),
                ('humidity', models.FloatField(verbose_name='humidity')),
            ],
        ),
        migrations.AddField(
            model_name='impressions',
            name='keyword',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='impressions', to='predict.Keyword', verbose_name='keyword'),
        ),
        migrations.AddField(
            model_name='adgroup',
            name='campaign',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='adgroups', to='predict.Campaign'),
        ),
    ]
