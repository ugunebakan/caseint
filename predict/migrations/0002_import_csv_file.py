from django.db import migrations
from predict.helpers import csv_to_dict


class Migration(migrations.Migration):
    def import_csv_data(apps, schema_editor):
        Account = apps.get_model('predict', 'Account')
        AdGroup = apps.get_model('predict', 'AdGroup')
        Campaign = apps.get_model('predict', 'Campaign')
        Keyword = apps.get_model('predict', 'Keyword')
        Impressions = apps.get_model('predict', 'Impressions')

        for value in csv_to_dict('another_df.csv'):

            account, created = Account.objects.get_or_create(
                name=value.get('accountId'),
            )

            campaign, created = Campaign.objects.get_or_create(
                name=value.get('campaignId'),
                account= account,
            )

            ad_group, created = AdGroup.objects.get_or_create(
                name=value.get('adgroupId'),
                campaign=campaign,
            )


            keyword, created = Keyword.objects.get_or_create(
                keyword=value.get('keywordId'),
            )

            impressions, created = Impressions.objects.get_or_create(
                ad_group = ad_group,
                keyword=keyword,
                clicks=value.get('clicks'),
                impressions=value.get('impressions'),
                date=value.get('day'),
            )

    dependencies = [
        ('predict', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(import_csv_data),
    ]
