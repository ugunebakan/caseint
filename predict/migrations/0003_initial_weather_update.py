from django.db import migrations
from predict.helpers import forecast_weather


class Migration(migrations.Migration):
    def initial_weather(apps, schema_editor):
        Impressions = apps.get_model('predict', 'Impressions')
        Weather = apps.get_model('predict', 'Weather')

        date_list = list(
            set(
                Impressions.objects.all().values_list(
                    'date', flat=True
                )
            )
        )

        for date in date_list:
            currently = forecast_weather(date).get('currently')
            _dict = {
                'date': date,
                'temperature': currently.get('temperature'),
                'uv_index': currently.get('uvIndex'),
                'humidity': currently.get('humidity')
            }

            Weather.objects.get_or_create(**_dict)



    dependencies = [
        ('predict', '0002_import_csv_file'),
        ('django_celery_beat', '0005_add_solarschedule_events_choices_squashed_0009_merge_20181012_1416'),
    ]

    operations = [
        migrations.RunPython(initial_weather),
    ]
