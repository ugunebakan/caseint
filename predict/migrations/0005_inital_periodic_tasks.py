from django.db import migrations


class Migration(migrations.Migration):
    def initial_periodic_task(apps, schema_editor):
        CrontabSchedule = apps.get_model(
            'django_celery_beat', 'CrontabSchedule')
        PeriodicTask = apps.get_model('django_celery_beat', 'PeriodicTask')

        # crontab is running in every5 minutes, but it must be optimized.
        crontab_schedule = CrontabSchedule.objects.create(minute='*/5')
        PeriodicTask.objects.create(
            task='predict.tasks.update_predictions',
            crontab=crontab_schedule,
            name='prediction',
        )

    dependencies = [
        ('predict', '0003_initial_weather_update'),
        ('django_celery_beat',
         '0005_add_solarschedule_events_choices_squashed_0009_merge_20181012_1416'), ]

    operations = [
        migrations.RunPython(initial_periodic_task),
    ]
