import csv
import requests
import requests_cache
from django.conf import settings


# for large scale applications memcache, redis cache, elastic cache etc.
# can be used
requests_cache.install_cache(
    'predict_cache',
    backend='memory',
)


def csv_to_dict(filename):
    # https://gist.github.com/ugunebakan/a376b38d9c4ffd7798f5b1bca6a12f28
    with open(filename, mode='r') as file:
        reader = csv.reader(file)
        headers = next(reader)
        return [dict(zip(headers, rows)) for rows in reader]


def forecast_weather(date):
    # I did not use darkskylib because it does not have
    # caching mechanism. We can later contribute the project with
    # caching.

    isodate = date.strftime('%s')
    LOCATION = ('59.3293', '18.0686')
    _API_URL = 'https://api.darksky.net/forecast'
    KEY = settings.DARK_SKY_API_KEY
    latitude, longitude = LOCATION

    url = '{url}/{key}/{latitude},{longitude},{date}?units=auto&exclude=hourly,daily,flags,offset'.format(
        url=_API_URL,
        key=KEY,
        latitude=latitude,
        longitude=longitude,
        date=isodate
    )

    request_params = {
        'headers': {'Accept-Encoding': 'gzip'},
    }

    response = requests.get(url, **request_params)
    return response.json()
